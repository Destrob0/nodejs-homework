const validator = require("validator");

exports.fighter = {
  id: "",
  name: {
    type: "String",
    validator: validator.isAlpha,
    errorMessage: "Name must contain only letters",
  },
  health: {
    type: "Constant",
    value: 100,
  },
  power: {
    type: "Number",
    validator: validator.isInt,
    min: 0,
    max: 100,
    errorMessage: "Power must be have in range [0..100]",
  },
  defense: {
    type: "Number",
    validator: validator.isInt,
    min: 0,
    max: 10,
    errorMessage: "Defense must be have in range [0..10]",
  },
};
