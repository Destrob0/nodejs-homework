import React,{Component} from 'react';
import './character.css';

export default class Character extends Component {
    state = {
        name: '',
        health: 0,
        power: 0,
        defense: 0
    }

    componentDidMount = () => {
        const {name,health,power,defense} = this.props;

        this.setState({name,health,power,defense});
    }

    render() {

    }
}