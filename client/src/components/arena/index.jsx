import React,{Component} from 'react'
import HealthBar from './../healthBar'

import './arena.css'

export default class Arena extends Component {
    state = {
        fighter1 : null,
        fighter2 : null
    }

    componentDidMount(){
        const {fighter1,fighter2} = this.props;
        this.setState({fighter1,fighter2})
    }

    render() {
        const {fighter1,fighter2} = this.props;
      
        return (
            <div className="arena___root">
                <div className="arena___fight-status">
                    <HealthBar name={fighter1.name}/>    
                    <div className="arena___versus-sign"></div>
                    <HealthBar name={fighter2.name}/>  
                </div>
                <div className="arena___battlefield">
                </div>             
            </div>
        )
    }
}