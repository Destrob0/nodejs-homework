import React from 'react'
import './Bar.css';

const HealthBar = (props) => {
    return (
        <div className="arena___fighter-indicator"> 
        <span className="arena___fighter-name">{props.name}</span>      
            <div className="arena___health-indicator">
                <div className="arena___health-bar"></div>
            </div>
        </div>
    )
}

export default HealthBar