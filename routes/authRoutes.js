const { Router } = require("express");
const AuthService = require("../services/authService");
const { responseMiddleware } = require("../middlewares/response.middleware");
const { sanitizeID, wrapper } = require("../services/dataService");

const router = Router();

router.post(
  "/login",
  async (req, res, next) => {
    try {
      // TODO: Implement login action (get the user if it exist with entered credentials)

      const data = await wrapper(sanitizeID)(AuthService.login)({
        email: req.body.email,
      });

      console.log(data);

      if (data.password === req.body.password)
        res.data = { email: data.email, password: data.password };
      else throw Error("Password isn`t correct");
    } catch (err) {
      res.err = err.message;
    } finally {
      next();
    }
  },
  responseMiddleware
);

module.exports = router;
