const sanitizeID = data => {
  if (typeof data === Array) {
    return data.map(elem => {
      const { id, ...body } = elem;
      return body;
    });
  }

  const { id, ...body } = data;

  return body;
};

const wrapper = func2 => func1 => data => func2(func1(data));

module.exports = {
  sanitizeID,
  wrapper
};
