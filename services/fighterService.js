const { FighterRepository } = require("../repositories/fighterRepository");

class FighterService {
  // TODO: Implement methods to work with fighters
  search(search) {
    const item = FighterRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }

  create(user) {
    const item = FighterRepository.create(user);
    if (!item) {
      return null;
    }
    return item;
  }

  update(id, fighterToUpdate) {
    const item = FighterRepository.update(id, fighterToUpdate);
    if (!item) {
      return null;
    }
    return item;
  }

  delete(id) {
    FighterRepository.delete(id);
  }

  getAll() {
    const items = FighterRepository.getAll();
    if (!items) {
      return null;
    }
    return items;
  }

  checkFields(data, body, model) {
    let error = "";
    switch (model[data].type) {
      case "String":
        error += model[data].validator(body[data])
          ? ""
          : model[data].errorMessage;
        break;
      case "Number":
        error += model[data].validator(body[data].toString(), {
          min: model[data].min,
          max: model[data].max,
        })
          ? ""
          : model[data].errorMessage;
        break;
    }
    return error;
  }
}

module.exports = new FighterService();
