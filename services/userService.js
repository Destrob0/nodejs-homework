const { UserRepository } = require("../repositories/userRepository");

class UserService {
  // TODO: Implement methods to work with user

  search(search) {
    const item = UserRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }

  create(user) {
    const item = UserRepository.create(user);
    if (!item) {
      return null;
    }
    return item;
  }

  update(id, userToUpdate) {
    const item = UserRepository.update(id, userToUpdate);
    if (!item) {
      return null;
    }
    return item;
  }

  delete(id) {
    UserRepository.delete(id);
  }

  getAll() {
    const items = UserRepository.getAll();
    if (!items) {
      return null;
    }
    return items;
  }

  checkFields(data, body, model) {
    let error = "";
    switch (model[data].type) {
      case "String":
        error += model[data].validator(body[data])
          ? ""
          : model[data].errorMessage;
        break;
      case "Number":
        error += model[data].validator(body[data], { min: model[data].min })
          ? ""
          : model[data].errorMessage;
        break;
      case "Email":
        error += model[data].validator(body[data], model[data].pattern)
          ? ""
          : model[data].errorMessage;
        break;
      case "Phone":
        error += model[data].validator(body[data], model[data].code)
          ? ""
          : model[data].errorMessage;
        break;
    }
    return error;
  }
}

module.exports = new UserService();
